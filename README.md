# Домашнее задание № 5. OTUServer
Курс "Python Developer. Professional"

## Описание сервиса

Сервис представляет собой HTTP-сервер, понимающий запросы GET и HEAD.
Сервер использует асинхронный метод обработки запросов, основанный на `epoll`. 
Сервер может быть распараллелен на несколько обработчиков запросов, для этого используется `multiprocessing`.

Запуск сервиса: `$ python httpd.py [-a HOST] [-p PORT] -r PATH/TO/DOCUMENT/DIRECTORY [-w NUMBER_OF_WORKERS] [-l PATH/TO/LOG]`

Аргументы командной строки:
- `--host`, `-a`: хост, по умолчанию `localhost`
- `--port`, `-p`: порт, по умолчанию `8080`
- `--document-root`, `-r`: путь до директории с файлами, которые отдаёт сервер, обязательный аргумент
- `--workers`, `-w`: количество воркеров, необязательный аргумент; если не указано или указано неположительное число, то равен числу CPU в системе
- `--log`, `-l`: путь к файлу логов, по умолчанию пишется в `stderr`

## Юнит-тестирование

Для тестирования использовался фреймворк `pytest`

Запуск тестов: `$ coverage run -m pytest tests/unit && coverage report -m`

Запуск юнит-тестирования настроен как пайплайн `unit-test`

## End-to-end-тестирование

Запуск end-to-end-тестирования настроен как пайплайн `end-to-end-test`

## Нагрузочное тестирование

Нагрузочное тестирование выполнялось через Apache Benchmarking Tool (`ab`).

Параметры тестирования:
- 50000 запросов
- 100 запросов одновременно
- запросы на `/httptest/dir2/`, чтобы запрос выполнялся успешно

### Результаты на персональной машине
```
$ ab -n 50000 -c 100 -r http://127.0.0.1:8080/
This is ApacheBench, Version 2.3 <$Revision: 1843412 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient)
Completed 5000 requests
Completed 10000 requests
Completed 15000 requests
Completed 20000 requests
Completed 25000 requests
Completed 30000 requests
Completed 35000 requests
Completed 40000 requests
Completed 45000 requests
Completed 50000 requests
Finished 50000 requests


Server Software:        OTUServer
Server Hostname:        127.0.0.1
Server Port:            8080

Document Path:          /
Document Length:        34 bytes

Concurrency Level:      100
Time taken for tests:   6.699 seconds
Complete requests:      50000
Failed requests:        0
Total transferred:      8650000 bytes
HTML transferred:       1700000 bytes
Requests per second:    7463.56 [#/sec] (mean)
Time per request:       13.398 [ms] (mean)
Time per request:       0.134 [ms] (mean, across all concurrent requests)
Transfer rate:          1260.93 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   0.8      0       9
Processing:     0   13  11.1     10      92
Waiting:        0   12  10.8      9      91
Total:          0   13  11.1     11      92
WARNING: The median and mean for the initial connection time are not within a normal deviation
        These results are probably not that reliable.

Percentage of the requests served within a certain time (ms)
  50%     11
  66%     15
  75%     18
  80%     20
  90%     27
  95%     34
  98%     46
  99%     55
 100%     92 (longest request)
```

### Результаты в пайплайне GitLab

Запуск нагрузочного тестирования настроен как пайплайн `load-test`.
