import pytest

from project.http_proto.http_response import (HTTP_RESPONSE_COMMENT,
                                              HTTPResponseBuilder,
                                              HTTPResponseCode)


def test_set_code():
    response_builder = HTTPResponseBuilder()
    for code in HTTPResponseCode:
        response_builder.set_response_code(code)
        assert response_builder.code == code
        assert response_builder.comment == HTTP_RESPONSE_COMMENT[code]
        response_builder.build()


@pytest.mark.parametrize('body', [b'', b'foo', 'привет мир'.encode('utf-8')])
def test_set_body_with_content_length(body):
    response_builder = HTTPResponseBuilder()
    response_builder.set_body(body, set_content_length=True)
    assert response_builder.body == body
    assert response_builder.headers['Content-Length'] == str(len(body))


@pytest.mark.parametrize('body', [b'', b'foo', 'привет мир'.encode('utf-8')])
def test_set_body_without_content_length(body):
    response_builder = HTTPResponseBuilder()
    response_builder.set_body(body, set_content_length=False)
    assert response_builder.body == body
    assert 'Content-Length' not in response_builder.headers
    response_builder.add_header('content-length', str(len(body)))
    assert response_builder.headers['content-length'] == str(len(body))
