import pytest

from project.http_proto import (HTTPRequest, HTTPRequestBuilder,
                                HTTPRequestParseError)


@pytest.fixture(
    params=[
        (
            'GET / HTTP/1.0\r\n'
            '\r\n',
            HTTPRequest(method='GET', target='/', version='HTTP/1.0')
        ),
        (
            'GET /hello world.html HTTP/1.0\r\n'
            '\r\n',
            HTTPRequest(method='GET', target='/hello world.html', version='HTTP/1.0')
        ),
        (
            'GET /hello world.html HTTP/1.0\r\n'
            'User-Agent: Mozilla/5.0 (X11; Linux i686; rv:2.0.1) Gecko/20100101 Firefox/4.0.1\r\n'
            '\r\n',
            HTTPRequest(
                method='GET', target='/hello world.html', version='HTTP/1.0',
                headers={'user-agent': 'Mozilla/5.0 (X11; Linux i686; rv:2.0.1) Gecko/20100101 Firefox/4.0.1'}
            )
        ),
        (
            'GET / HTTP/1.0\r\n'
            'Content-Type: text/plain\r\n'
            'Content-Length: 13\r\n'
            '\r\n'
            'Hello, world!',
            HTTPRequest(
                method='GET', target='/', version='HTTP/1.0',
                headers={'content-length': '13', 'content-type': 'text/plain'},
                body=b'Hello, world!'
            )
        ),
        (
            'GET / HTTP/1.0\r\n'
            'Content-Type: text/plain\r\n'
            'Content-Length: 13\r\n'
            '\r\n'
            'Hello, world! How are you?',
            HTTPRequest(
                method='GET', target='/', version='HTTP/1.0',
                headers={'content-length': '13', 'content-type': 'text/plain'},
                body=b'Hello, world!'
            )
        ),
        (
            'GET / HTTP/1.0\r\n'
            'Content-Type: text/plain\r\n'
            '\r\n'
            'Hello, world!',
            HTTPRequest(
                method='GET', target='/', version='HTTP/1.0',
                headers={'content-type': 'text/plain'}
            )
        ),
        (
            'GET / HTTP/1.0\r\n'
            'Content-Type: text/plain\r\n'
            'Content-Length: 19\r\n'
            '\r\n'
            'привет мир',
            HTTPRequest(
                method='GET', target='/', version='HTTP/1.0',
                headers={'content-type': 'text/plain', 'content-length': '19'},
                body=b'\xd0\xbf\xd1\x80\xd0\xb8\xd0\xb2\xd0\xb5\xd1\x82 \xd0\xbc\xd0\xb8\xd1\x80'
            )
        ),
        (
            'GET / HTTP/1.0\r\n'
            'Content-Type: text/plain\r\n'
            'Content-Length: 29\r\n'
            '\r\n'
            'Hello, world!\r\n\r\nHow are you?',
            HTTPRequest(
                method='GET', target='/', version='HTTP/1.0',
                headers={'content-length': '29', 'content-type': 'text/plain'},
                body=b'Hello, world!\r\n\r\nHow are you?'
            )
        )
    ]
)
def data_pass(request):
    return request.param


def test_http_request_fullbuild_pass(data_pass):
    request_string, request_gt = data_pass
    request_builder = HTTPRequestBuilder()
    request_builder.add_chunk(request_string.encode(encoding='utf-8'))
    assert request_builder.headers_received
    assert request_builder.request_received
    request = request_builder.build()
    assert request.method == request_gt.method
    assert request.target == request_gt.target
    assert request.version == request_gt.version
    assert request.body == request_gt.body
    assert request.headers == request_gt.headers


def test_http_request_iterbuild_pass(data_pass):
    request_gt = data_pass[1]
    request_builder = HTTPRequestBuilder()
    for c in data_pass[0]:
        request_builder.add_chunk(c.encode(encoding='utf-8'))
    assert request_builder.headers_received
    assert request_builder.request_received
    request = request_builder.build()
    assert request.method == request_gt.method
    assert request.target == request_gt.target
    assert request.version == request_gt.version
    assert request.body == request_gt.body
    assert request.headers == request_gt.headers


@pytest.fixture(
    params=[
        '\r\n'
        '\r\n',

        'GET\r\n'
        '\r\n',

        'GET /\r\n'
        '\r\n',

        'GET / HTTP/1.0\r\n'
        'malformed header\r\n'
        '\r\n',

        'GET / HTTP/1.0\r\n'
        'Content-Length: qr\r\n'
        '\r\n',
    ]
)
def data_fail(request):
    return request.param


def test_http_request_fullbuild_fail(data_fail):
    request_builder = HTTPRequestBuilder()
    with pytest.raises(HTTPRequestParseError):
        request_builder.add_chunk(data_fail.encode('utf-8'))


def test_http_request_iterbuild_fail(data_fail):
    request_builder = HTTPRequestBuilder()
    with pytest.raises(HTTPRequestParseError):
        for c in data_fail:
            request_builder.add_chunk(c.encode(encoding='utf-8'))
