from pathlib import Path

import pytest

from project.http_proto import HTTPRequestBuilder, HTTPRequestParseError
from project.server.request_handler import RequestHandler


@pytest.fixture(scope='module')
def mock_server():
    class S:
        SETTINGS = {
            'HTTP_PROTOCOL_VERSION': 'HTTP/9000',
            'HTTP_RESPONSE_HEADERS': {
                'Server': 'OTUServer',
                'Connection': 'close'
            },
            'document_root': Path(__file__).parent.parent / 'data'
        }

    return S()


def test_get_protocol_version(mock_server):
    request_handler = RequestHandler(mock_server, HTTPRequestBuilder())
    assert request_handler.get_server_prorocol_version() == 'HTTP/9000'


def test_set_protocol_version_default(mock_server):
    request_handler = RequestHandler(mock_server, HTTPRequestBuilder())
    assert request_handler.response_builder.version != 'HTTP/9000'
    request_handler.set_response_protocol_version()
    assert request_handler.response_builder.version == 'HTTP/9000'


def test_set_protocol_version(mock_server):
    request_handler = RequestHandler(mock_server, HTTPRequestBuilder())
    assert request_handler.response_builder.version != 'HTTP/000'
    request_handler.set_response_protocol_version('HTTP/000')
    assert request_handler.response_builder.version == 'HTTP/000'


def test_headers_default(mock_server):
    request_handler = RequestHandler(mock_server, HTTPRequestBuilder())
    request_handler.set_response_headers()
    assert request_handler.response_builder.headers['Server'] == 'OTUServer'
    assert request_handler.response_builder.headers['Connection'] == 'close'
    assert 'Date' in request_handler.response_builder.headers


def test_headers_fail(mock_server):
    request_handler = RequestHandler(mock_server, HTTPRequestBuilder())
    with pytest.raises(ValueError):
        request_handler.set_response_headers('Foo', 'bar', 'Baz')


def test_headers_user(mock_server):
    request_handler = RequestHandler(mock_server, HTTPRequestBuilder())
    request_handler.set_response_headers('Foo', 'bar', 'Egg', 'ham')
    assert request_handler.response_builder.headers['Server'] == 'OTUServer'
    assert request_handler.response_builder.headers['Connection'] == 'close'
    assert 'Date' in request_handler.response_builder.headers
    assert request_handler.response_builder.headers['Foo'] == 'bar'
    assert request_handler.response_builder.headers['Egg'] == 'ham'


@pytest.mark.parametrize('body', [b'', b'foo', 'привет мир'.encode('utf-8')])
def test_set_body_with_content_length(mock_server, body):
    request_handler = RequestHandler(mock_server, HTTPRequestBuilder())
    request_handler.set_response_body(body, set_content_length=True)
    assert request_handler.response_builder.body == body
    assert request_handler.response_builder.headers['Content-Length'] == str(len(body))


@pytest.mark.parametrize('body', [b'', b'foo', 'привет мир'.encode('utf-8')])
def test_set_body_without_content_length(mock_server, body):
    request_handler = RequestHandler(mock_server, HTTPRequestBuilder())
    request_handler.set_response_body(body, set_content_length=False)
    assert request_handler.response_builder.body == body
    assert 'Content-Length' not in request_handler.response_builder.headers
    request_handler.set_response_headers('content-length', str(len(body)))
    assert request_handler.response_builder.headers['content-length'] == str(len(body))
