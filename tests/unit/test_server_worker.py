import pytest

from project.server import worker


@pytest.fixture
def server_worker():
    return worker.Worker(("localhost", 8080))


def test_activate_deactivate(server_worker):
    server_worker.activate()
    assert len(server_worker.connections) == 1
    server_worker.deactivate()
    assert len(server_worker.connections) == 0
