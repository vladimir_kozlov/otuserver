import argparse
import multiprocessing as mp


class CLArguments:
    host: str
    port: int
    document_root: str
    workers: int
    log: str

    def __init__(self, args):
        self.host = args.host
        self.port = args.port
        self.document_root = args.document_root
        self.workers = args.workers if args.workers > 0 else mp.cpu_count()
        self.log = args.log


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", "-a", default='localhost',
                        help="server host, defaults to %(default)s")
    parser.add_argument("--port", "-p", type=int, default=8080,
                        help="server port, defaults to %(default)d")
    parser.add_argument("--document-root", "-r", required=True,
                        help="path to document directory")
    parser.add_argument("--workers", "-w", type=int, default=mp.cpu_count(),
                        help="number of workers, defaults to number of CPUs available (%(default)s on this machine)")
    parser.add_argument('--log', '-l',
                        help="path to log file; if not specified, log will be written to stderr")

    args = parser.parse_args()
    return CLArguments(args)
