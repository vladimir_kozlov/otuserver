import logging
import mimetypes
from pathlib import Path

from .parse_args import parse_args
from .server import Server

logger = logging.getLogger(__name__)


def main(host, port, document_root, workers):
    server = Server(host=host, port=port, document_root=document_root, workers=workers)
    try:
        logger.info("Starting server...")
        server.serve_forever()
    except KeyboardInterrupt:
        logger.info("Stopping server...")
    logger.info('Server stopped')


if __name__ == "__main__":
    mimetypes.init([Path(__file__).parent / 'mime.types'])
    args = parse_args()
    logging.basicConfig(
        filename=args.log,
        format="[%(asctime)s at process %(processName)s] %(levelname)s: %(message)s",
        level=logging.INFO,
    )
    main(args.host, args.port, args.document_root, args.workers)
