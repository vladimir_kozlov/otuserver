import multiprocessing as mp
import signal
from pathlib import Path

from .worker import Worker


def run_worker(host: str, port: int, settings: dict[str]):
    worker = Worker(address=(host, port), settings=settings)

    class StopWorker(Exception):
        pass

    def sigterm_handler(signum, frame):
        raise StopWorker

    worker.activate()
    signal.signal(signal.SIGTERM, sigterm_handler)
    while True:
        try:
            worker.poll_connections()
        except StopWorker:
            break
    signal.signal(signal.SIGTERM, signal.SIG_IGN)
    worker.deactivate()


class Server:
    host: str
    port: int
    document_root: str
    workers: int

    HTTP_PROTOCOL_VERSION = 'HTTP/1.0'
    HTTP_RESPONSE_HEADERS = {
        'Connection': 'close',
        'Server': 'OTUServer'
    }

    def __init__(self, host: str, port: int, document_root: str, workers: int):
        self.host = host
        self.port = port
        self.document_root = document_root
        self.workers = workers
        if not Path(document_root).is_dir():
            raise ValueError(f'{document_root} is not a valid path to directory')

    def serve_forever(self):
        settings = {
            'document_root': self.document_root,
            'HTTP_RESPONSE_HEADERS': self.HTTP_RESPONSE_HEADERS,
            'HTTP_PROTOCOL_VERSION': self.HTTP_PROTOCOL_VERSION
        }
        with mp.Pool(
                processes=self.workers,
                initializer=signal.signal, initargs=(signal.SIGINT, signal.SIG_IGN),
                maxtasksperchild=1
        ) as pool:
            pool.starmap(run_worker, [(self.host, self.port, settings)] * self.workers)
