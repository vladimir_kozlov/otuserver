import select
import socket
from dataclasses import dataclass
from enum import Enum
from typing import Optional

from ..http_proto import HTTPRequestBuilder as HTTPRequestBuffer
from ..http_proto import HTTPResponse as HTTPResponseBuffer
from .request_handler import RequestHandler


class ConnectionStatus(str, Enum):
    OPEN = 'OPEN'
    CLOSED = 'CLOSED'
    COMPLETE = 'COMPLETE'


@dataclass
class Connection:
    server: any

    socket: socket.socket
    address: any

    request: HTTPRequestBuffer
    response: Optional[HTTPResponseBuffer]
    handler: RequestHandler

    def is_server(self) -> bool:
        return self.socket.fileno() == self.server.server_socket.fileno()

    @staticmethod
    def is_ready_to_read(event: int) -> bool:
        return bool(event & select.EPOLLIN)

    @staticmethod
    def is_ready_to_write(event: int) -> bool:
        return bool(event & select.EPOLLOUT)

    def read(self, size: int = 1024) -> ConnectionStatus:
        request = self.request
        chunk = self.socket.recv(size)

        # if requests hasn't been completely parsed yet, set status to OPEN
        # if requests has been completely parsed, set status to COMPLETE
        # if socket is available for read but returns 0 bytes, then connection is closed by client, set status to CLOSED
        if chunk:
            return ConnectionStatus.COMPLETE if request.add_chunk(chunk) else ConnectionStatus.OPEN
        else:
            return ConnectionStatus.CLOSED

    def handle_request(self):
        self.response = self.handler.handle_request()

    def can_write(self) -> bool:
        return not self.response.is_sent

    def write(self):
        chunk = self.response.to_send
        bytes_sent = self.socket.send(chunk)
        self.response.register_sent(bytes_sent)

    def close(self):
        self.socket.close()
