from datetime import datetime
from mimetypes import guess_type as guess_mimetype
from pathlib import Path
from urllib.parse import unquote as path_unquote
from urllib.parse import urlparse as path_parse

from project.http_proto import (HTTPRequest, HTTPRequestBuilder,
                                HTTPRequestParseError, HTTPResponse,
                                HTTPResponseBuilder, HTTPResponseCode)


class RequestHandler:
    request_builder: HTTPRequestBuilder
    response_builder: HTTPResponseBuilder

    def __init__(self, server, request: HTTPRequestBuilder):
        self.server = server
        self.request_builder = request
        self.response_builder = HTTPResponseBuilder()

    @property
    def response(self):
        return self.response_builder.build()

    def get_server_prorocol_version(self) -> str:
        return self.server.SETTINGS['HTTP_PROTOCOL_VERSION']

    def get_server_response_headers(self) -> dict[str]:
        return self.server.SETTINGS['HTTP_RESPONSE_HEADERS']

    def get_auto_response_headers(self) -> dict[str]:
        return {
            'Date': datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')
        }

    def set_response_protocol_version(self, protocol_version: str = None):
        self.response_builder.set_protocol_version(protocol_version or self.get_server_prorocol_version())
        return self

    def set_response_code(self, code: HTTPResponseCode):
        self.response_builder.set_response_code(code)
        return self

    def set_response_body(self, body: bytes, set_content_length=False):
        self.response_builder.set_body(body, set_content_length=set_content_length)
        return self

    def set_response_headers(self, *args):
        if len(args) % 2:
            raise ValueError('Values passed to function should form pairs but number of values is odd')
        handler_response_headers = {}
        for i in range(0, len(args), 2):
            header_name = args[i]
            header_value = args[i + 1]
            handler_response_headers[header_name] = header_value

        for header_name, header_value in {
            **self.get_server_response_headers(),
            **self.get_auto_response_headers(),
            **(handler_response_headers or {})
        }.items():
            self.response_builder.add_header(header_name, header_value)
        return self

    def respond_bad_request(self, error: HTTPRequestParseError):
        return (
            self.set_response_protocol_version()
            .set_response_code(HTTPResponseCode.BAD_REQUEST)
            .set_response_body(str(error).encode('utf-8'), set_content_length=True)
            .set_response_headers('Content-Type', 'text/plain')
        )

    def respond_not_allowed(self, method: str):
        return (
            self.set_response_protocol_version()
            .set_response_code(HTTPResponseCode.NOT_ALLOWED)
            .set_response_body(f'Method {method} not allowed'.encode('utf-8'), set_content_length=True)
            .set_response_headers('Content-Type', 'text/plain')
        )

    def respond_forbidden(self, target: str):
        return (
            self.set_response_protocol_version()
            .set_response_code(HTTPResponseCode.FORBIDDEN)
            .set_response_body(f'Access to resource at {target} forbidden'.encode('utf-8'), set_content_length=True)
            .set_response_headers('Content-Type', 'text/plain')
        )

    def respond_not_found(self, target: str):
        return (
            self.set_response_protocol_version()
            .set_response_code(HTTPResponseCode.NOT_FOUND)
            .set_response_body(f'Resource at {target} not found'.encode('utf-8'), set_content_length=True)
            .set_response_headers('Content-Type', 'text/plain')
        )

    def handle_GET(self, request: HTTPRequest):
        target = path_parse(request.target).path
        root = Path(self.server.SETTINGS['document_root']).resolve()
        path_requested = (root / path_unquote(target.lstrip('/'), encoding='utf-8')).resolve()
        if root != path_requested and root not in path_requested.parents:
            # hack attempt
            self.respond_forbidden(target)
            return
        if not path_requested.exists():
            self.respond_not_found(target)
            return
        if path_requested.is_dir():
            if (path_requested / 'index.html').exists() and (path_requested / 'index.html').is_file():
                self.handle_GET(
                    HTTPRequest(target=str(Path(target) / 'index.html'))
                )
            else:
                self.respond_not_found(str(Path(target) / 'index.html'))
        else:
            if not target.endswith('/'):
                with open(path_requested, 'rb') as fp:
                    body = fp.read()

                self.set_response_protocol_version() \
                    .set_response_code(HTTPResponseCode.OK) \
                    .set_response_body(body, set_content_length=True) \
                    .set_response_headers('Content-Type', guess_mimetype(path_requested)[0])
            else:
                # dir requested but it's a regular file
                self.respond_not_found(target)

    def handle_HEAD(self, request: HTTPRequest):
        target = path_parse(request.target).path
        root = Path(self.server.SETTINGS['document_root']).resolve()
        path_requested = (root / path_unquote(target.lstrip('/'), encoding='utf-8')).resolve()
        if root != path_requested and root not in path_requested.parents:
            # hack attempt
            self.respond_forbidden(target)
            return
        if not path_requested.exists():
            self.respond_not_found(target)
            return
        if path_requested.is_dir():
            if (path_requested / 'index.html').exists() and (path_requested / 'index.html').is_file():
                self.handle_HEAD(
                    HTTPRequest(target=str(Path(target) / 'index.html'))
                )
            else:
                self.respond_not_found(str(Path(target) / 'index.html'))
        else:
            if not target.endswith('/'):
                self.set_response_protocol_version() \
                    .set_response_code(HTTPResponseCode.OK) \
                    .set_response_body(b'') \
                    .set_response_headers(
                    'Content-Type', guess_mimetype(path_requested)[0],
                    'Content-Length', path_requested.stat().st_size
                )
            else:
                # dir requested but it's a regular file
                self.respond_not_found(target)

    def handle_request(self) -> HTTPResponse:
        request = self.request_builder.build()

        if request.method == 'GET':
            self.handle_GET(request)
        elif request.method == 'HEAD':
            self.handle_HEAD(request)
        else:
            self.respond_not_allowed(request.method)
        return self.response
