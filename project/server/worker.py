import logging
import select
import socket
from typing import Optional

from ..http_proto import HTTPRequestBuilder as HTTPRequestBuffer
from ..http_proto import HTTPRequestParseError
from .connection import Connection, ConnectionStatus
from .request_handler import RequestHandler

logger = logging.getLogger('worker')


class Worker:
    address: tuple[str, int]

    server_socket: Optional[socket.socket] = None
    epoll: Optional[select.epoll] = None

    connections: dict[int, Connection]

    SETTINGS: dict[str]

    def __init__(self, address: tuple[str, int], settings: dict[str] = None):
        self.address = address
        self.SETTINGS = settings or {}

    def activate(self):
        logger.info('Starting worker...')
        self.server_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        self.server_socket.bind(self.address)
        self.server_socket.setblocking(False)
        self.server_socket.listen()
        self.epoll = select.epoll()
        self.epoll.register(self.server_socket.fileno(), eventmask=select.EPOLLIN)

        self.connections = {
            self.server_socket.fileno(): Connection(
                server=self,
                socket=self.server_socket,
                address=self.address,
                request=None,
                response=None,
                handler=None
            )
        }
        logger.info('Worker successfully started')

    def deactivate(self):
        logger.info('Stopping worker...')
        for fd, connection in self.connections.items():
            self.epoll.unregister(fd)
            connection.close()
        self.epoll.close()
        self.epoll = None
        self.connections = {}
        self.server_socket = None
        logger.info('Worker successfully stopped')

    def create_new_connection(self):
        client_socket, address = self.server_socket.accept()
        logger.info("Establishing connection with %s:%d...", address[0], address[1])
        fd = client_socket.fileno()
        self.epoll.register(fd, eventmask=select.EPOLLIN)
        request = HTTPRequestBuffer()
        self.connections[fd] = Connection(
            server=self,
            socket=client_socket,
            address=address,
            request=request,
            response=None,
            handler=RequestHandler(
                server=self,
                request=request
            )
        )
        logger.info("Connection with %s:%d established.", address[0], address[1])

    def switch_from_read_to_write(self, fd: int):
        self.epoll.modify(fd, select.EPOLLOUT)

    def cancel_connection(self, fd: int):
        connection = self.connections[fd]
        address = connection.address
        logger.info("Terminating connection with %s:%d...", address[0], address[1])
        self.epoll.unregister(fd)
        connection.socket.close()
        del self.connections[fd]
        logger.info("Connection with %s:%d terminated.", address[0], address[1])

    def poll_connections(self):
        for fd, event in self.epoll.poll(timeout=1., maxevents=-1):
            connection = self.connections[fd]
            if connection.is_server():
                self.create_new_connection()
            elif connection.is_ready_to_read(event):
                try:
                    read_status = connection.read()
                    if read_status == ConnectionStatus.CLOSED:
                        self.cancel_connection(fd)
                    if read_status == ConnectionStatus.COMPLETE:
                        connection.handle_request()
                except HTTPRequestParseError as e:
                    read_status = ConnectionStatus.COMPLETE
                    connection.response = connection.handler.respond_bad_request(e).response

                if read_status == ConnectionStatus.COMPLETE:
                    self.switch_from_read_to_write(fd)
            elif connection.is_ready_to_write(event):
                if connection.can_write():
                    connection.write()
                else:
                    self.cancel_connection(fd)
