from enum import Enum


class HTTPResponseCode(int, Enum):
    OK = 200
    FORBIDDEN = 403
    NOT_FOUND = 404
    NOT_ALLOWED = 405
    BAD_REQUEST = 400


HTTP_RESPONSE_COMMENT = {
    HTTPResponseCode.OK: "OK",
    HTTPResponseCode.FORBIDDEN: "Forbidden",
    HTTPResponseCode.NOT_FOUND: "Not Found",
    HTTPResponseCode.NOT_ALLOWED: "Not Allowed",
    HTTPResponseCode.BAD_REQUEST: "Bad Request"
}


class HTTPResponse:
    raw: bytes
    position: int

    def __init__(self, raw: bytes):
        self.raw = raw
        self.position = 0

    @property
    def is_sent(self):
        return self.position == len(self.raw)

    def register_sent(self, sent: int):
        self.position += sent

    @property
    def to_send(self) -> bytes:
        return self.raw[self.position:]


class HTTPResponseBuilder:
    version: str
    code: HTTPResponseCode
    comment: str

    headers: dict[str, str]

    body: bytes

    def __init__(self):
        self.reset()

    def reset(self):
        self.set_protocol_version('HTTP/1.0')
        self.set_response_code(HTTPResponseCode.NOT_ALLOWED)
        self.headers = {}
        self.set_body(b'')

    def set_protocol_version(self, version: str):
        self.version = version

    def set_response_code(self, code: HTTPResponseCode):
        self.code = code
        self.comment = HTTP_RESPONSE_COMMENT[code]

    def set_body(self, body: bytes, set_content_length=False):
        self.body = body
        if set_content_length:
            self.add_header('Content-Length', str(len(self.body)))

    def add_header(self, header_name: str, header_value: str):
        self.headers[header_name] = header_value

    def build(self) -> HTTPResponse:
        status = f'{self.version} {self.code} {self.comment}'
        headers = [f'{header_name}: {header_value}' for header_name, header_value in self.headers.items()]
        raw = b'\r\n'.join(
            [
                *[
                    bytes(s, encoding='utf-8')
                    for s in [status, *headers, '']
                ],
                self.body
            ]
        )
        self.reset()
        return HTTPResponse(raw=raw)
