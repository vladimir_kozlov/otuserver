class HTTPRequestParseError(Exception):
    pass


class HTTPRequest:
    method: str
    target: str
    version: str

    headers: dict[str, str]

    body: bytes

    def __init__(
        self,
        method: str = None,
        target: str = None,
        version: str = None,
        headers: dict = None,
        body: bytes = None
    ):
        self.method = method or ''
        self.target = target or ''
        self.version = version or ''
        self.headers = headers or {}
        self.body = body or b''


class HTTPRequestBuilder:
    raw: bytes
    request: HTTPRequest
    headers_received: bool
    request_received: bool

    def __init__(self):
        self.reset()

    def reset(self):
        self.raw = b''
        self.request = HTTPRequest()
        self.request_received = False
        self.headers_received = False

    def _parse_message(self, message: bytes) -> tuple[str, str, str]:
        delim = ' '
        try:
            message = message.decode(encoding='latin1')
        except UnicodeDecodeError as e:
            raise HTTPRequestParseError("Malformed HTTP request: HTTP message not decoded from byte string") from e

        split_pos = message.find(delim)
        if split_pos == -1:
            raise HTTPRequestParseError(f"Malformed HTTP request: HTTP method not found in HTTP message line {message}")
        method = message[:split_pos]
        message = message[split_pos + len(delim):]

        split_pos = message.rfind(delim)
        if split_pos == -1:
            raise HTTPRequestParseError(
                f"Malformed HTTP request: HTTP protocol version not found in HTTP message line {message}"
            )
        version = message[split_pos + len(delim):]
        message = message[:split_pos]

        target = message

        return method, target, version

    def _parse_header(self, header: bytes) -> tuple[str, str]:
        delim = ':'
        try:
            header = header.decode(encoding="latin1")
        except UnicodeDecodeError as e:
            raise HTTPRequestParseError("Malformed HTTP request: HTTP header not decoded from byte string") from e
        split_pos = header.find(delim)
        if split_pos == -1:
            raise HTTPRequestParseError(f"Malformed HTTP request: HTTP header not parsed parsed from {header}")

        header_name, header_value = header[:split_pos].lower(), header[split_pos + len(delim):].lstrip()
        return header_name, header_value

    def build(self) -> HTTPRequest:
        request = self.request
        self.reset()
        return request

    def add_chunk(self, chunk: bytes) -> bool:
        self.raw += chunk

        if not self.headers_received:
            delim = b'\r\n\r\n'
            split_pos = self.raw.find(delim)
            if split_pos == -1:
                # this is fine, headers are not fully transferred yet
                return False
            # headers are here! Now, to parse them
            self.headers_received = True
            raw = self.raw[split_pos + len(delim):]

            delim = b'\r\n'
            message_headers = self.raw[:split_pos].split(delim)
            message = message_headers[0]
            headers = message_headers[1:]
            self.raw = raw

            self.request.method, self.request.target, self.request.version = self._parse_message(message)
            for header in headers:
                header_name, header_value = self._parse_header(header)
                if header_name in self.request.headers:
                    raise HTTPRequestParseError(f'Malformed HTTP request: repeating HTTP header {header_name}')
                self.request.headers[header_name] = header_value

        try:
            expected_body_length = int(self.request.headers.get('content-length', '0'))
        except ValueError as e:
            raise HTTPRequestParseError(
                f'Malformed HTTP request: Content-Length not parsed from {self.request.headers["content-length"]}'
            ) from e
        if len(self.raw) >= expected_body_length:
            self.request.body = self.raw[:expected_body_length]
            self.request_received = True
            return True
        else:
            return False
